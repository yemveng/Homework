
	CREATE TABLE tb_categories ( 
	  id INT PRIMARY KEY auto_increment,
	  name VARCHAR (100) not null
	);
	
	CREATE TABLE tb_articles(
		 id INT PRIMARY KEY auto_increment,
		 title VARCHAR NOT NULL, 
		 description TEXT NOT NULL, 
		 author VARCHAR NOT NULL,
		 created_date VARCHAR,
		thumbnail VARCHAR NOT NULL,
		category_id INT REFERENCES tb_categories(id) on DELETE CASCADE on update CASCADE
	);
