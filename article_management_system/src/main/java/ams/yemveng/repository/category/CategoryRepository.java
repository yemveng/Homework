package ams.yemveng.repository.category;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import ams.yemveng.model.Category;

@Repository
public interface CategoryRepository {
	
	@Select("SELECT id, name FROM tb_categories ORDER BY id ASC")
	public	List<Category> findall();
	@Select("SELECT id, name FROM tb_categories WHERE id=#{id}")
	public  Category findOne(int id);
	@Delete("DELETE FROM tb_cagetgories WHERE id=#{id}")
	public boolean delete(int id);
	@Delete("Update  tb_cagetgories set name=#{name} WHERE id=#{id}")
	public boolean update(Category category);
	@Insert("insert into (name) Values(#{name})")
	public boolean save(Category category);
	
}
 