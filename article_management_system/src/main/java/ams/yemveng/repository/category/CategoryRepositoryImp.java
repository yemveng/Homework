package ams.yemveng.repository.category;

import java.util.ArrayList;
import java.util.List;

//import org.springframework.stereotype.Repository;

import ams.yemveng.model.Category;

//@Repository
public class CategoryRepositoryImp  implements CategoryRepository{

	
	private List<Category> categories=new ArrayList<>();
	
	public CategoryRepositoryImp() {
		
		categories.add(new Category(1,"Spring"));
		categories.add(new Category(2,"Java"));
		categories.add(new Category(3,"Web"));
		categories.add(new Category(4,"Korean"));
	}
	
	@Override
	public List<Category> findall() {
		return categories;
	}

	@Override
	public Category findOne(int id) {
		for (Category c : categories) {
			if(c.getId()==id) {
				return c;
			}
		}
		return null;
	}

	@Override
	public boolean delete(int id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean update(Category category) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean save(Category category) {
		// TODO Auto-generated method stub
		return false;
	}
	
}
