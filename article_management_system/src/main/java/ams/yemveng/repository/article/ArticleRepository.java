package ams.yemveng.repository.article;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.web.bind.annotation.ResponseBody;

import ams.yemveng.model.Article;


@ResponseBody()
public interface ArticleRepository {

	@Insert("INSERT INTO tb_articles(title, description, author, thumbnail, created_date,  category_id) VALUES(#{title} ,#{description}, #{author}, #{thumbnail}, #{createdDate}, #{category.id})")
	public void add(Article article) ;
	
	@Select("SELECT  a.id, a.title, a.description, a.author, a.created_date, a.thumbnail, a.category_id, c.name AS category_name FROM tb_articles a INNER JOIN tb_categories c ON a.category_id= c.id WHERE a.id=#{id}")
	@Results({
			@Result(property="id", column="id"),
			@Result(property="title", column="title"),
			@Result(property="description", column="description"),
			@Result(property="author", column="author"),
			@Result(property="createdDate", column="created_date"),
			@Result(property="thumbnail", column="thumbnail"),
			@Result(property="category.id", column="category_id"),
			@Result(property="category.name", column="category_name")	
	})
	public Article fineOne(int id);
	
	@Select("SELECT  a.id, a.title, a.description, a.author, a.created_date, a.thumbnail, a.category_id, c.name FROM tb_articles a INNER JOIN tb_categories c ON a.category_id= c.id ORDER BY a.id ASC")
	@Results({
//		@Result(property="id", column="id"),
//		@Result(property="title", column="title"),
//		@Result(property="description", column="descrption"),
//		@Result(property="author", column="author"),
		@Result(property="createdDate", column="created_date"),
//		@Result(property="thumbnail", column="thumbnail"),
		@Result(property="category.id", column="category_id"),
		@Result(property="category.name", column="name")	
	})
	public List<Article> findAll();
	
	
	@Delete("Delete From tb_articles where id=#{id} ")
	public void delete(int id);
	
	
	@Update("Update tb_articles set title=#{title},  author=#{author},description=#{description}, thumbnail=#{thumbnail}, created_date=#{createdDate},  category_id=#{category.id} where id=#{id}")

	public void update(Article article);
}
