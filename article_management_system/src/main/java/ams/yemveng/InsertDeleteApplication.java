package ams.yemveng;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InsertDeleteApplication {

	public static void main(String[] args) {
		SpringApplication.run(InsertDeleteApplication.class, args);
	}
}
