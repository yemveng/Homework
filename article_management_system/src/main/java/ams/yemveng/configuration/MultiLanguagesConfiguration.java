package ams.yemveng.configuration;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

@Configuration
@PropertySource("classpath:ams.properties")
public class MultiLanguagesConfiguration extends WebMvcConfigurerAdapter{
	 @Value("${languages.baseNames}")
	private String[] baseName;
	
	@Bean(name = "localeResolver")
	public LocaleResolver localResolver() {
		SessionLocaleResolver slr=new SessionLocaleResolver();
		slr.setDefaultLocale(Locale.US);
//		slr.setDefaultLocale(new Locale("kh"));
		return slr;
	}
	@Bean
	public LocaleChangeInterceptor localeChangeInterceptor() {
		LocaleChangeInterceptor lci=new LocaleChangeInterceptor();
		lci.setParamName("lang");
		return lci;
	}
	 @Override
	 
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(this.localeChangeInterceptor());
	}
	 @Bean
	 public MessageSource messageSource() {
		 ResourceBundleMessageSource msg=new ResourceBundleMessageSource();
		 msg.setBasenames(baseName);
		 msg.setDefaultEncoding("UTF-8");
		 return msg; 
	 }	 
}

