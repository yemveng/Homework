package ams.yemveng.service.category;

import java.util.List;

import ams.yemveng.model.Category;

public interface CategoryService {
	public List<Category> findAll();
	public Category findOne(int id);
	public boolean update(Category category);
	public boolean delete(int id);
	public boolean save(Category category);
}
