package ams.yemveng.service.category;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ams.yemveng.model.Category;
import ams.yemveng.repository.category.CategoryRepository;

@Service
public class CategoryImp implements CategoryService{

	@Autowired
	private CategoryRepository categoryRepo;
	
	
	@Override
	public List<Category> findAll() {
		return categoryRepo.findall();
	}

	@Override
	public Category findOne(int id) {
	
		return categoryRepo.findOne(id);
	}

	@Override
	public boolean update(Category category) {
		// TODO Auto-generated method stub
		boolean status=categoryRepo.update(category);
		if(status) {
			System.out.println("Udpate Success");
		}
		return false;
	}

	@Override
	public boolean delete(int id) {
		boolean status=categoryRepo.delete(id);
		if(status) {
			System.out.println("Delete Success");
		}
		return false;
	}

	@Override
	public boolean save(Category category) {
		categoryRepo.save(category);
		return false;
	}

	
}
