package ams.yemveng.service;

import java.util.List;

import ams.yemveng.model.Article;

public interface ArticleService {
	void add(Article article) ;
	Article fineOne(int id);
	List<Article> findAll();
	void delete(int id);
	void update(Article article);
	void view(Article article);
}
