package ams.yemveng.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ams.yemveng.model.Article;
import ams.yemveng.repository.article.ArticleRepository;

@Service
public class ArticleServiceImp implements ArticleService{

	@Autowired
	private ArticleRepository articleRepo;
	
	
	@Override
	public void add(Article article) {
		  
		articleRepo.add(article);
		
	}

	@Override
	public Article fineOne(int id) {
		return articleRepo.fineOne(id);
	}

	@Override
	public List<Article> findAll() {
		return articleRepo.findAll();
	}
	
	@Override
	public void delete(int id) {
		articleRepo.delete(id);
	}
	@Override
	public void update(Article article) {
		articleRepo.update(article);
	}

	@Override
	public void view(Article article) {
		// TODO Auto-generated method stub
		
	}

	

}
