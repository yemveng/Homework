package ams.yemveng.Controllers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.validation.Valid;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import ams.yemveng.model.Article;
import ams.yemveng.service.ArticleService;
import ams.yemveng.service.category.CategoryService;

@Controller

public class ActicleController {

	@Autowired
	private ArticleService articleService;
	@Autowired
	private CategoryService categoryService;
		
	String serverPath="/Users/DELL/Documents/FileUpload/";
	@GetMapping("/article")
		public String acticle(ModelMap map) {
		map.addAttribute("articles", articleService.findAll());
		return "article";
	}
	@GetMapping("/add")
	public String add(ModelMap map) {
		map.addAttribute("article", new Article());
		map.addAttribute("categories",categoryService.findAll());
		map.addAttribute("formAdd", true);
		return "add";
	}

	@PostMapping("/add")	
	public String saveArticle(@RequestParam("image") MultipartFile thumbnail, @Valid @ModelAttribute Article article, BindingResult result,ModelMap map) 
	{
	if(result.hasErrors()) {
		System.out.println("Binding ERROR");
		map.addAttribute("article", article);
		map.addAttribute("categories", categoryService.findAll());
		map.addAttribute("formAdd", true);
		return "add";
	  }
	

	if(thumbnail.isEmpty()) {
		System.out.println("File Empty");
		return "add";
	}else {
		
		try {
			Files.copy(thumbnail.getInputStream(), Paths.get(serverPath, thumbnail.getOriginalFilename()));
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
	article.setThumbnail("/image/" + thumbnail.getOriginalFilename());
	article.setCategory(categoryService.findOne(article.getCategory().getId()));
	article.setCreatedDate(new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
	articleService.add(article);
	System.out.println(article);
	return "redirect:/article";
}

	@GetMapping("/delete/{id}")
	public String delete(@PathVariable("id") int id) {
		System.out.println(id);
	articleService.delete(id);
	return "redirect:/article";
   }
	@GetMapping("/update/{id}")
	public String updates(@PathVariable int id, ModelMap map) {
		map.addAttribute("article",articleService.fineOne(id));
		map.addAttribute("categories",categoryService.findAll());
		map.addAttribute("formAdd", false);
		return "add";
	}
	
	@PostMapping("/update")
	public String saveUpdate(@RequestParam("image") MultipartFile thumbnail,@ModelAttribute Article article) {
	//	article.setCreatedDate(new Date().toString());
		if(!thumbnail.isEmpty()) {
			
			try {
				Files.copy(thumbnail.getInputStream(), Paths.get(serverPath, thumbnail.getOriginalFilename()));
			} catch (IOException e) {
				e.printStackTrace();
			} 
			article.setThumbnail("/image/" + thumbnail.getOriginalFilename());
		}else {
			article.setThumbnail(article.getThumbnail());
		}
		
		article.setCategory(categoryService.findOne(article.getCategory().getId()));
//		article.setThumbnail(serverPath);
		article.setCreatedDate(new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
		articleService.update(article);
		return "redirect:/article";
	 }

//	form view

	@GetMapping("view/{id}")
	public String view(@PathVariable int id, Model m){
		Article article=this.articleService.fineOne(id);
		m.addAttribute("article",article);
		return "view";
		
	}
}

