package ams.yemveng.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

import ams.yemveng.service.category.CategoryService;

@Controller
public class CategoryController {
	
	private CategoryService categoryService;
	CategoryController(CategoryService categoryService){
		this.categoryService=categoryService;
	}
	
	@GetMapping("/category/list")
	public String categorList(ModelMap m) {
		m.addAttribute("categories",categoryService.findAll());
		System.out.println(categoryService.findAll());
		return "category/list";
	}

}
